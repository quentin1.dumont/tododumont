package com.example.tododumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class AddTodo extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_todo);
    }

    public void onClick(View view) {
        if (view.getId() == R.id.save) {
            EditText editText = findViewById(R.id.input);
            String text = editText.getText().toString();
            if (!text.isEmpty()) {
                Intent intent = new Intent();
                intent.putExtra("text", text);
                setResult(RESULT_OK, intent);
                finish();
            }
        } else if (view.getId() == R.id.cancel) {
            finish();
        }
    }
}