package com.example.tododumont;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {
    private ArrayAdapter<String> adapter;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<String> entries = new ArrayList<>();
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_checked, entries);
        listView = findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        loadList();
        setSupportActionBar(findViewById(R.id.toolbar));
    }

    public void addTodo(View view) {
        Intent intent = new Intent(this, AddTodo.class);
        startActivityForResult(intent, 1);
    }

    public void delete(MenuItem item) {
        for (int i = adapter.getCount() - 1; i >= 0; i--) {
            if (listView.isItemChecked(i)) {
                adapter.remove(adapter.getItem(i));
            }
        }
        listView.clearChoices();
        adapter.notifyDataSetChanged();
    }

    public void saveList(MenuItem item) {
        try {
            FileOutputStream fileOutputStream = openFileOutput(String.valueOf(R.string.filename), MODE_PRIVATE);
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream);
            for (int i = 0; i < adapter.getCount(); i++) {
                outputStreamWriter.write(adapter.getItem(i) + "|" + listView.isItemChecked(i) + "\n");
            }
            outputStreamWriter.close();
            Toast.makeText(this, R.string.saved, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, R.string.save_error, Toast.LENGTH_LONG).show();
        }
    }

    private void loadList() {
        try {
            FileInputStream fileInputStream = openFileInput(String.valueOf(R.string.filename));
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String task = line.split("\\|")[0];
                boolean checked = Boolean.parseBoolean(line.split("\\|")[1]);
                adapter.add(task);
                if (checked) {
                    listView.setItemChecked(adapter.getCount() - 1, true);
                }
            }
            bufferedReader.close();
            adapter.notifyDataSetChanged();
        } catch (Exception ignored) {}
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && data.hasExtra("text")) {
            String text = data.getStringExtra("text");
            adapter.add(text);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater= getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }
}